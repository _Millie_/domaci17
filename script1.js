$ ('#myform').validate ({
    rules: {
        firstname: {
            minlength: 5,
            maxlength: 10,
            },

        lastname: {
            required: true,
            },

        jobtype:  {
            required: true,
        },

        minsalary:  {
            required: true,
            max: 2000
        },

        age:  {
            required: true,
            max: 99,
            min: 18
        },

        email: {
            required: true,
            email:true
        }
    },
    messages: {
        firstname: {
            minlength: jQuery.validator.format("Min 5 caracter!"),
            maxlength: jQuery.validator.format("Max 10 caracter!")
        },

        lastname: {
            required: "Please enter last name!",
        },


        email: {
            required: "We need your email address to contact you!",
            email: "Your email address must be in the format of name@domain.com"
        },

        jobtype: {
            required: "Please select job type",
            
        },

        minsalary: {
            max: jQuery.validator.format("Max 2000 !")
        },

        age: {
            min: jQuery.validator.format("Min 18 years!"),
            max: jQuery.validator.format("Max 99 years!")
        },
    }

});